<?php
return [
    'api_version' => 2,
    'config_dir' => 'novum.bri',
    'namespace' => 'ApiNovumBri',
    'protocol' => 'any',
    'live_domain' => 'api.belastingdienst.demo.novum.nu',
    'test_domain' => 'api.test.belastingdienst.demo.novum.nu',
    'dev_domain' => 'api.belastingdienst.innovatieapp.nl'

];
