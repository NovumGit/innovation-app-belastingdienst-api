echo "Starting on tcp port: 50010"
docker run --rm \
--name outway_apinovumbri \
--volume ~/platform/system/public_html/api.belastingdienst.demo.novum.nu/nlx/root.crt:/certs/root.crt:ro \
--volume ~/platform/system/public_html/api.belastingdienst.demo.novum.nu/nlx/org.crt:/certs/org.crt:ro \
--volume ~/platform/system/public_html/api.belastingdienst.demo.novum.nu/nlx/org.key:/certs/org.key:ro \
--env DIRECTORY_INSPECTION_ADDRESS=directory-inspection-api.demo.nlx.io:50020 \
--env TLS_NLX_ROOT_CERT=/certs/root.crt \
--env TLS_ORG_CERT=/certs/org.crt \
--env TLS_ORG_KEY=/certs/org.key \
--env DISABLE_LOGDB=1 \
--publish 50020:8080 \
nlxio/outway:latest